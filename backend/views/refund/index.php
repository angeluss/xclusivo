<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\controllers\RefundSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Refunds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="refund-index">

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute'=>'user_id',
                'content'=>function($data){
                    return $data->user->username;
                }
            ],
            // 'user_email:email',
            'transaction_id',
            'amount',
            // 'description:ntext',
            // 'admin_comment:ntext',
            'created_at:datetime',
            'updated_at:datetime',
            'status',

            [
              'class' => 'yii\grid\ActionColumn',
              'template' => '{view}',
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
