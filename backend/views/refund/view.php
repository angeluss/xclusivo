<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\Refund */

$this->title = 'Refund #'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Refunds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="refund-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'transaction_id',
            [
              'label' => 'User',
              'value'=> $model->user->username
            ],
            'user_email:email',
            'amount',
            'description:ntext',
            'admin_comment:ntext',
            'created_at:datetime',
            'updated_at:datetime',
            'status',
        ],
    ]) ?>

<?php
    if ($model->isPending()) {
?>
      <div class="chat-form">
          <h3>Process refund</h3>
          <?php $form = ActiveForm::begin(); ?>
          <?= Html::textarea('message', '', ['rows' => 3, 'style' => 'width:90%', 'placeholder' => 'write a comment']); ?>
          <div class="form-group">
            Select action
            <select name="status">
              <option value=""></option>
              <option value="1">Confirm refund</option>
              <option value="2">Cancel refund</option>
            </select>
          </div>
          <div class="form-group">
              <?= Html::submitButton('Save', ['class' =>'btn btn-success']) ?>
          </div>
          <?php ActiveForm::end(); ?>
      </div>
<?php
    }
    ?>

</div>
