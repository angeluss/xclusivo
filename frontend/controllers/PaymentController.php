<?php

namespace frontend\controllers;


use Yii;
use common\models\User;
use frontend\models\Advertiser;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use frontend\actions\AdvertiserEventFeedAction;
use frontend\actions\BookAction;
use common\models\Refund;
use common\helpers\Payment;
use common\helpers\Toolbox;


class PaymentController extends Controller
{

    public $layout = 'nonadvertiser';



    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
//						'actions' => ['skrill', 'skrill-redirect'],
                        'actions' => ['index', 'refund'],
                        'roles' => ['@'],
                    ],
                /*
                  [
                  'allow'	 => true,
                  'actions' => ['manage'],
                  'roles'	 => ['Advertiser'],
                  ],
                 * 
                 */
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'refund' => \frontend\actions\CreateRefundAction::className(),
        ];
    }


    public function actionIndex($amount = 100.00)
    {
        $errors = false;

        if (!Toolbox::currentUser()) {
            throw new \yii\web\HttpException(403, 'Access Denied.');
        }

        if (Yii::$app->request->isPost) {

            if (empty($_POST['stripeToken']) || empty($_POST['amount'])) {
                throw new \yii\web\HttpException(400, 'Bad Request.');
            }

            $token = $_POST['stripeToken'];
            $amount = $_POST['amount'];
            try {
                if (!Toolbox::currentUser()->stripe_id) {
                    $customer = Yii::$app->stripe->createCustomer(Toolbox::currentUser()->email, $token);
                    Toolbox::currentUser()->stripe_id = $customer->id;
                    Toolbox::currentUser()->save(false, ['stripe_id']);
                } else {
                    $customer = Yii::$app->stripe->retrieveCustomer(Toolbox::currentUser()->stripe_id);
                    $customer->source = $token;
                    $customer->save();
                }

                $charge = Yii::$app->stripe->createCharge($customer->id, round($amount*100), 'usd', null, [
                    'description' => Payment::REASON_REFILL,
                ]);
            } catch (\Exception $ex) {
                Yii::$app->session->setFlash('error', $ex->getMessage());
                $this->refresh();
                Yii::$app->end();
            }

            $t = Payment::createRefillTransaction($amount, Toolbox::currentUser(), $charge->id, \common\models\Transaction::GATEWAY_STRIPE, true);
            $t->commit();
            Yii::$app->session->setFlash('success', Yii::t('booking', 'Payment {sum} successfully done. Your funds refilled.', ['sum' => Toolbox::formatMoney($amount)]));
            $this->refresh();
            Yii::$app->end();

            //Array ( [_csrf] => SWtlZzFlRG8aAFMpQjcRVnAuLlAAAwYMexMuPlojKl4zNB9fRFFxHg== [amount] => 100 [stripeToken] => src_18tQOeBHaS5ztdCHdXLq0F0k )
        }

        $refund = new Refund();
        $refund->user_id = Toolbox::currentUser()->id;
        $refund->user_email = Toolbox::currentUser()->email;
        $params = ['amount' => $amount, 'errors' => $errors, 'refund' => $refund];
        return $this->render('index', $params);
    }

    /*
      public function actionIndex($amount = 100.00)
      {
      $errors = false;
      if (Yii::$app->request->isPost) {
      if (empty($_POST['amount']) || empty($_POST['payment_method_nonce'])) {
      throw new \yii\web\BadRequestHttpException();
      }

      $amount = $_POST['amount'];
      $btt = Yii::$app->braintree->doTransactionSale(floatval($_POST['amount']), $_POST['payment_method_nonce']);

      if (!$btt) {
      $errors = Yii::$app->braintree->errors;
      } elseif (Yii::$app->braintree->isStatusValid($btt->status)) {
      $t = Payment::createRefillTransaction($amount, Toolbox::currentNonAdvertiser(), $btt->id, 'Braintree', true);
      $t->commit();
      Yii::$app->session->setFlash('success', Yii::t('booking', 'Payment {sum} successfully done.', ['sum' => Toolbox::formatMoney($amount)]));
      $this->refresh();
      Yii::$app->end();
      } else {
      //TODO: fail on braintree side
      }
      }
      $refund = new Refund();
      $refund->user_id = Toolbox::currentUser()->id;
      $refund->user_email = Toolbox::currentUser()->email;
      $params = ['amount' => $amount,'errors' => $errors, 'refund' => $refund];
      return $this->render('index', $params);
      }
     * 
     */

    /*
      public function actionSkrill($amount = null)
      {
      $params = [];
      $params['model'] = $model = Yii::$app->skrill->createForm(['amount' => $amount ? $amount : 100]);
      $model->validate();

      return $this->render('skrill', $params);
      }


      public function actionSkrillRedirect()
      {
      if (Yii::$app->request->isPost) {
      Yii::$app->skrill->getSessionId($_POST);
      }
      }
     * 
     */




    /*
      public function beforeAction($action)
      {
      if ($action->id === 'index') {
      //$this->enableCsrfValidation = false;
      }

      return parent::beforeAction($action);
      }
     * 
     */

}


