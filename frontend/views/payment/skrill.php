<?php
/*
  
// 
use zvook\Skrill\Models\QuickCheckout;
use zvook\Skrill\Forms\QuickCheckoutForm;

$quickCheckout = new QuickCheckout([
    'pay_to_email' => 'grayfolk@gmail.com',
    'amount' => 123,
    'currency' => 'EUR',
]);

$form = new QuickCheckoutForm($quickCheckout);

echo $form->open([
    'class' => 'skrill-form'
]);

$exclude = [];
echo $form->renderHidden($exclude);
//<input type="text" name="amount"> .....
echo $form->renderSubmit('Pay', ['class' => 'btn']);
echo $form->close();
 * 
 */
?>

<?php
$form = \yii\widgets\ActiveForm::begin([
    'id' => 'payment-form',
//	'action' => Yii::$app->skrill->actionUrl,
	'action' => \yii\helpers\Url::to(['payment/skrill-redirect']),
    'options' => [],
]) 
?>
<?= $model->renderHiddenFields(); ?>
 <?= yii\helpers\Html::submitButton(Yii::t('app', 'Process Payment'), ['class' => 'btn btn-green']); ?>
<?php \yii\widgets\ActiveForm::end() ?>