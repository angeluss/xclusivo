<?php

use Yii;
use yii\helpers\Url;
use common\helpers\Toolbox;
use frontend\widgets\NonAdvInfo;
use frontend\widgets\AdvInfo;
use yii\widgets\ActiveForm;
use common\helpers\Payment;


//Yii::$app->braintree->jsRegister($this);
//Yii::$app->braintree->jsInitForm($this, 'payment-form');
common\assets\ProjectAsset::register($this);
?>

<div class="user-col">
    <?php if (Toolbox::currentNonAdvertiser()) : ?>
        <?=
        NonAdvInfo::widget([
            'user' => Toolbox::currentNonAdvertiser(),
            'payments' => []
        ]);
        ?>
    <?php elseif (Toolbox::currentAdvertiser()) : ?>
        <?php
        echo AdvInfo::widget([
            'id' => Toolbox::currentUser()->id,
            'advertiser' => Toolbox::currentAdvertiser()
        ]);
        ?>
    <?php endif; ?>
</div>
<div class="booking-col">
    <?php if (Toolbox::currentNonAdvertiser()) : ?>
        <h2>Refill Balance</h2>
        <?php
        $form = ActiveForm::begin([
            'action' => Url::to(['payment/index']),
            'method' => 'POST',
            'id' => "stripe-checkout",
        ]);
        ?>
        <label for="amount">Amount</label>
        <input type="text" name="amount" value="<?= $amount; ?>" id="amount">
        <input type="hidden" value="" name="stripeToken" id="stripe-token">
        <button id='checkoutButton'>Checkout</button>
        <!--
    <form action="<?= Url::to(['payment/index']); ?>" method="POST" id="stripe-checkout">
        
        <script
            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
            data-key="<?= Yii::$app->stripe->publicKey; ?>"
            data-amount="<?= str_replace('.', '', Toolbox::formatMoney($amount, false)); ?>"
            data-name="Xclusivo"
            data-description="<?= \common\helpers\Payment::REASON_REFILL; ?>"
            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
            data-label="Pay with Card or Bitcoin"
            data-locale="auto"
            data-currency="usd"
            data-email="<?= Toolbox::currentNonAdvertiser()->email; ?>"
            data-bitcoin="true">
        </script>            
    </form>
        -->

        <?php ActiveForm::end(); ?>

        <script src="https://checkout.stripe.com/checkout.js"></script>

        <script>
            var handler = StripeCheckout.configure({
                key: '<?= Yii::$app->stripe->publicKey; ?>',
                locale: 'auto',
                name: 'Xclusivo',
                currency: "usd",
                bitcoin: true,
                email: '<?= Toolbox::currentNonAdvertiser()->email; ?>',
                description: '<?= \common\helpers\Payment::REASON_REFILL; ?>',
                token: function (token)
                {
                    $('#stripe-token').val(token.id);
                    $('#stripe-checkout').submit();
                }
            });

        </script>

        <?php $this->registerJs(" 
            $('#checkoutButton').on('click', function (e)
            {
                e.preventDefault();
                e.stopPropagation();

                var amount = $('input#amount').val();
                amount = amount.replace(/\$/g, '').replace(/\,/g, '')

                amount = parseFloat(amount);

                if (isNaN(amount)) {
                    project.flashMessage('".Yii::t('booking', 'Please enter a valid amount in USD (point separated number).')."', project.FM_ERROR);
                } else if (amount < ".Payment::minRefill().") {
                    project.flashMessage('".Yii::t('booking', 'Refilling amount must be at least {amount}.', ['amount' => Toolbox::formatMoney(Payment::minRefill())])."', project.FM_ERROR);
                } else {
                    amount = amount * 100; // Needs to be an integer!
                    handler.open({
                        amount: Math.round(amount)
                    })
                }
            });
            
        ", yii\web\View::POS_LOAD) ;?>
        
        <?php /*
          <form action="https://test.bitpay.com:443/checkout" method="post" >
          <input type="hidden" name="action" value="checkout" />
          <input type="hidden" name="posData" value="" />
          <input type="hidden" name="data" value="wDXnm1gtekYETA+gK5XPNXjAMhgEr3HHItQNt4riSbujEwFfVvE5qTnPLlnh8yovJdJbghWXK+w895WG1mE7vYifcwnwfI8hOLr5E5plNObaXqUa2fMn2EHVx+2fPaxfi7tpNgormhuci/NplkQzpLS1jrN3wM/6Uopxa5Wt71/53NerxtXuLvAQzyVwm6nMM0yHC8tS/dRr8AxpT14r+w==" />
          <input type="image" src="https://test.bitpay.com:443/img/button2.png" border="0" name="submit" alt="BitPay, the easy way to pay with bitcoins." >
          </form>
         * 
         */ ?>

        <?php /*
          <!--
          <form method="post" id="payment-form" action="<?= Url::to(['payment/index']); ?>">
          <?php if ($errors) : ?>
          <section>
          Errors:<br>
          <?php foreach ($errors as $e) : ?>
          <p><?php echo $e->message; ?></p>
          <?php endforeach; ?>
          </section>
          <?php endif; ?>
          <section>
          <label for="amount">
          <span class="input-label">Amount</span>
          <div class="input-wrapper amount-wrapper">
          <input id="amount" name="amount" type="tel" min="1" placeholder="Amount" value="<?= $amount; ?>">
          </div>
          </label>
          <div class="bt-drop-in-wrapper">
          <div id="bt-dropin"></div>
          </div>
          </section>

          <button class="button" type="submit"><span>Test Transaction</span></button>
          </form>
          -->
         * 
         */ ?>
    <?php endif; ?>

    <h2>Refund</h2>
    <?= $this->render('_refund_form', ['model' => $refund]); ?>

    <h2>Your Transactions</h2>
    <?=
    \frontend\widgets\UserPayments::widget([
        'user' => Toolbox::currentUser(),
        'viewMode' => null, //Full data view mode
        'providerOptions' => [
            'pagination' => ['pageSize' => 9],
        ],
        'gridOptions' => [
            'layout' => '{items}{pager}',
            'ajaxUpdate' => true,
        ],
    ]);
    ?>    
</div>