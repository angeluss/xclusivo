<?php

/**
 * @var $form \yii\widgets\ActiveForm
 */
use yii\helpers\Html;


$form = \yii\widgets\ActiveForm::begin([
    'id' => 'login-form',
    'action' => \yii\helpers\Url::to(['payment/refund']),
    'options' => ['class' => 'form-horizontal'],
    'enableAjaxValidation' => true,
]);
?>

<?= $form->field($model, 'amount')->textInput(['type' => 'number']); ?>
<?= $form->field($model, 'description')->textarea(); ?>
<?= Html::submitButton('Create Refund Request', ['class' => 'btn btn-green']); ?>

<?php \yii\widgets\ActiveForm::end(); ?>

