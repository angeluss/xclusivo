<?php

use frontend\models\AdvertiserMedia;
use yii\helpers\Html;
use common\helpers\Toolbox;
?> 
<h1><?= Html::a($adv->user->username, ['site/advertiser', 'id' => $adv->id], ['target' => '_blank']); ?></h1>
<div class="date">
	<?= Html::activeCheckbox($model, 'is_secure'); ?> 
</div>

<div class="date">
	<?= Toolbox::formatDateTime($model->from_date) . ' - ' . Toolbox::formatDateTime($model->to_date); ?> 
	(<?= $model->hoursBooked ?> <?= ($model->hoursBooked == 1) ? Yii::t('app', 'hour') : Yii::t('app', 'hours'); ?>)
</div>

<div class="date">
	<?= Yii::t('booking', 'Total Cost'); ?>: <b><?= Toolbox::formatMoney($model->total_cost); ?></b>
</div>

<div class="date">
	<p class="red"><?= Yii::t('app', 'Are you sure?'); ?></p>
</div>


<div class="girl-img">
	<?= Html::img(AdvertiserMedia::getDefaultPhotoUrl($adv->user_id, AdvertiserMedia::PREFIX_BIG_THUMB)); ?>
</div>
