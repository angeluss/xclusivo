<?php
use common\helpers\Toolbox;
?>
<div class="adv-booking-data js-adv-booking-data text-center">

	<p class=""> 
		<?= \yii\helpers\Html::a($model->user->username, common\helpers\Toolbox::userPublicUrl($model->user), ['target' => '_blank']); ?>
	</p>
	
	<p>
		<?= \yii\helpers\Html::activeCheckbox($model, 'no_show_guarantee'); ?>
	</p>

	<p class="booking-status booking-status-<?= strtolower($model->status); ?>"><?= Yii::t('app', $model->status); ?></p>

	<p class="nowrap">
		<?= common\helpers\Toolbox::formatDateTime($model->from_date); ?>
		-
		<?= common\helpers\Toolbox::formatDateTime($model->to_date); ?>
		<br>
		(<?= $model->hoursBooked ?> <?= ($model->hoursBooked == 1) ? Yii::t('app', 'hour') : Yii::t('app', 'hours'); ?>)		
	</p>

	<p class="nowrap">
		<?= Yii::t('booking', 'Total Cost'); ?>: <b><?= Toolbox::formatMoney($model->total_cost); ?></b>	
	<p>
		<?=
		\yii\helpers\Html::a(Yii::t('app', 'Confirm Booking'), '#', [
			'class' => 'btn btn-success js-booking-manage',
			'data-href' => \yii\helpers\Url::to(['booking/manage', 'id' => $model->id]),
			'data-action' => '1',
		]);
		?>
		<?=
		\yii\helpers\Html::a(Yii::t('app', 'Cancel (Remove) Booking'), '#', [
			'class' => 'btn btn-gray js-booking-manage',
			'data-href' => \yii\helpers\Url::to(['booking/manage', 'id' => $model->id]),
			'data-action' => '0',
		]);
		?>
	</p>

</div>

