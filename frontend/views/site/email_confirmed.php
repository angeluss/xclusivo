<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = \Yii::t('app', 'Confirm email');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p class="big-text"><?=Yii::t('app', $isOk ? 'Email has been confirmed. Now you can login.' : 'Email has not been confirmed. You can\'t login.');?></p>
</div>
