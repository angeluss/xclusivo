<?php

namespace frontend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "{{%like_photo}}".
 *
 * @property integer $media_id
 * @property integer $user_id
 *
 * @property Media $media
 * @property User $user
 */
class LikePhoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%like_photo}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['media_id', 'user_id'], 'required'],
            [['media_id', 'user_id'], 'integer'],
            [['media_id'], 'exist', 'skipOnError' => true, 'targetClass' => AdvertiserMedia::className(), 'targetAttribute' => ['media_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'media_id' => 'Media ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedia()
    {
        return $this->hasOne(AdvertiserMedia::className(), ['id' => 'media_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function isLiked($mediaId, $userId)
    {
      return self::find()->where(['media_id' => $mediaId])->andWhere(['user_id' => $userId])->count() > 0;
    }
}
