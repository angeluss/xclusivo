<?php

namespace frontend\widgets;


use common\models\User;
use yii\web\NotFoundHttpException;
use common\models\Booking;
use common\helpers\Toolbox;
use common\models\Transaction;
use common\models\TransactionQuery;
use yii\data\ActiveDataProvider;


class UserPayments extends \yii\bootstrap\Widget
{

    public $user;
    public $providerOptions = [];
    public $gridOptions = [];
    public $query = null;
    public $viewMode = null; // "short" supported only this moment



    public function init()
    {
        parent::init();

        if (!$this->user || !($this->user instanceof User)) {
            throw new \yii\base\Exception('User model required.');
        }

        if ($this->query && !($this->query instanceof TransactionQuery)) {
            throw new \yii\base\Exception('TransactionQuery required.');
        }
    }


    public function run()
    {
        $params = [
            'user' => $this->user,
            'gridOptions' => $this->gridOptions,
        ];

        if (!$this->query) {
            $this->query = Transaction::find()
                ->joinWith([
                    'sender' => function ($q) {
                        $q->from(['sender' => User::tableName()]);
                    },
                        'receiver' => function ($q) {
                        $q->from(['receiver' => User::tableName()]);
                    },])
                    ->byUser($this->user)
                    ->orderBy(Transaction::tableName() . '.created_at DESC');
            }


            $config = $this->providerOptions;
            $config['query'] = $this->query;
            $params['provider'] = $dp = new ActiveDataProvider($config);

            $viewName = 'UserPayments/index'.($this->viewMode ? '_'.$this->viewMode : ''); 
            return $this->render($viewName, $params);
        }


        /**
         * 
         * @return boolean
         */
        public function getIsOwn()
        {
            return (Toolbox::currentUser()) && (Toolbox::currentUser()->id == $this->user->id);
        }

    }


    