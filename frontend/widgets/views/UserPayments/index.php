<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\helpers\Toolbox;
use common\models\Transaction;


?>
<?php
echo GridView::widget([
    'dataProvider' => $provider,
    'emptyText' => Yii::t('app', 'No transactions found.'),
    'columns' => [
        [
            'attribute' => 'id',
            'enableSorting' => false,
        ],
        //     'external_id', //Not sure that it should be visible
        [
            'header' => Yii::t('booking', 'Sender'),
            'value' => function($model) use ($user) {
                if ($model->sender) {
                    return ($model->sender->id == $user->id) ? Html::tag('b', $model->sender->username) : Html::a($model->sender->username, Toolbox::userPublicUrl($model->sender));
                } else {
                    return Yii::t('app', 'XClusivo');
                }
                ;
            },
            'format' => 'html',
        ],
        [
            'header' => Yii::t('booking', 'Receiver'),
            'value' => function($model) use ($user) {
                if ($model->receiver) {
                    return ($model->receiver->id == $user->id) ? Html::tag('b', $model->receiver->username) : Html::a($model->receiver->username, Toolbox::userPublicUrl($model->receiver));
                } else {
                    return null;
                }
            },
            'format' => 'html',
        ],
        [
            'attribute' => 'Status',
            'enableSorting' => false,
            'format' => 'html',
            'value' => function ($model) {
                $color = false;

                switch ($model->status) {
                    case Transaction::STATUS_PENDING :
                        $color = 'CCCCCC';
                        break;
                    case Transaction::STATUS_COMPLETED:
                        $color = '33CC33';
                        break;
                    case Transaction::STATUS_CANCELLED:
                        $color = 'CC3333';
                        break;
                    case Transaction::STATUS_REVERTED:
                        $color = '3333CC';
                        break;
                }
                return Html::tag('span', $model->status, [
                    'class' => 'status-' . strtolower($model->status),
                    'style' => ($color) ? 'color: #' . $color . ';' : '',
                ]);
            },
        ],
        [
            'attribute' => 'amount',
            'enableSorting' => false,
            'format' => 'html',
            'value' => function ($model) use ($user) {
                if ($model->sender_id == $user->id) {
                    return Html::tag('span', '-' . Toolbox::formatMoney($model->amount), ['class' => 'money-minus', 'style' => 'color: red;']);
                } elseif ($model->receiver_id == $user->id) {
                    return Html::tag('span', '+' . Toolbox::formatMoney($model->amount), ['class' => 'money-plus', 'style' => 'color: green;']);
                } else {
                    return Toolbox::formatMoney($value);
                }
            },
        ],
        [
            'attribute' => 'reason',
            'enableSorting' => false,
        ],
        [
            'attribute' => 'created_at',
            'enableSorting' => false,
            'value' => function ($model) {
                return Toolbox::formatDateTime($model->created_at);
            },
        ],
        [
            'attribute' => 'updated_at',
            'enableSorting' => false,
            'value' => function ($model) {
                return ($model->created_at != $model->updated_at) ? Toolbox::formatDateTime($model->updated_at) : '-';
            },
        ],
    ],
]);
?>