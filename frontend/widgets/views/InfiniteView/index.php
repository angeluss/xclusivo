<?php
/**
 * Created by PhpStorm.
 * User: Ievgen
 * Date: 16.05.2016
 * Time: 14:45
 */
?>
<a name="results"></a>
<?php
if ($models) {
  foreach($models as $model) {
      echo $this->render('item', ['model'=>$model]);
  }
} else {
  echo '<div class="nothing-found">',Yii::t('app', 'Your search didn\'t match any profiles'),'</div>';
}
