<?php

namespace frontend\actions;


use Yii;
use yii\base\Action;
use yii\web\Request;
use yii\web\Response;
use common\models\User;
use frontend\models\Advertiser;
use common\models\Booking;
use common\helpers\Toolbox;
use common\helpers\Payment;


class BookingCodeAction extends Action
{

    public $viewName = 'code';



    public function behaviors()
    {
        return [
            \common\behaviors\AjaxBehavior::className(),
        ];
    }


    /**
     *
     * @param int $id Advertiser ID
     * @param int $cid Client (non-advertiser) user ID
     * @return string
     */
    public function run()
    {
        if (!Yii::$app->request->isAjax) {
            throw new \yii\web\HttpException(400, 'Invalid request');
        }

        if (!$adv = Toolbox::currentAdvertiser()) {
            throw new \yii\web\HttpException(403, 'Access denied.');
        }

        if (Yii::$app->request->isPost) {
            if (!$code = Yii::$app->request->post('code')) {
                return $this->renderAjax(false, Yii::t('booking', 'Secure code "{code}" is not found.', ['code' => $code]));
            }

            $booking = Booking::find()
                ->approved()
                ->andWhere(['secure_code' => $code])
                ->andWhere(['advertiser_id' => $adv->id])
                ->one();

            if (!$booking) {
                return $this->renderAjax(false, Yii::t('booking', 'Secure code "{code}" is not found.', ['code' => $code]));
            } else {
                $booking->commit();
                return $this->renderAjax(true, Yii::t('booking', 'Secure code accepted. You got {sum}.', ['sum' => Toolbox::formatMoney($booking->transaction->amount)]));
            }
        } else {
            return $this->controller->renderAjax($this->viewName);
        }
    }

}


