<?php

namespace frontend\actions;


use Yii;
use yii\base\Action;
use yii\web\Request;
use yii\web\Response;
use common\models\User;
use frontend\models\Advertiser;
use common\models\Booking;
use common\models\Refund;
use common\helpers\Toolbox;
use common\helpers\Payment;


class CreateRefundAction extends Action
{

    public $viewName = 'refund';



    public function behaviors()
    {
        return [
            \common\behaviors\AjaxBehavior::className(),
        ];
    }


    /**
     *
     * @param int $id User ID
     * @return string
     */
    public function run()
    {
        $user = Toolbox::currentUser();

        if (!$user) {
            throw new \yii\web\HttpException(403, 'Access denied');
        }

        $params = ['model' => $model = new Refund()];
        $model->user_id = $user->id;
        $model->user_email = $user->email;



        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return \yii\widgets\ActiveForm::validate($model);
            }
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('booking', 'Refund request for {amount} successfully created', [
                    'amount' => Toolbox::formatMoney($model->amount),
                ]));
                $this->controller->redirect(\yii\helpers\Url::to(['payment/index']));
            }
            
        } 
        
        return $this->controller->render($this->viewName, $params);
    }

}


