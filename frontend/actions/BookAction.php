<?php

namespace frontend\actions;


use Yii;
use yii\base\Action;
use yii\web\Request;
use yii\web\Response;
use common\models\User;
use frontend\models\Advertiser;
use common\models\Booking;
use common\helpers\Toolbox;
use common\helpers\Payment;


class BookAction extends Action
{


    public function behaviors()
    {
        return [
            \common\behaviors\AjaxBehavior::className(),
        ];
    }


    /**
     *
     * @param int $id Advertiser ID
     * @param int $cid Client (non-advertiser) user ID
     * @return string
     */
    public function run($id, $cid)
    {
        if (!Yii::$app->request->isAjax || !Yii::$app->request->isPost) {
            throw new \yii\web\HttpException(400, 'Invalid request');
        }

        $adv = Advertiser::find()->joinWith(['user'])->andWhere([Advertiser::tableName() . '.id' => $id])->one();
        $nonadv = User::find()->andWhere(['id' => $cid])->one();

        if (!$adv || !$nonadv) {
            throw new \yii\web\NotFoundHttpException();
        }

        $model = new Booking();
        $model->capturePost();
        $model->advertiser_id = intval($id);
        $model->user_id = intval($cid);
        $model->status = Booking::STATUS_PENDING;
        $model->total_cost = $model->hoursBooked * $adv->price;

        if (null === $model->is_secure) {
            $model->is_secure = true;
        }

        if ($model->ok) {
            if ($model->total_cost > $nonadv->balance) {
                Yii::$app->session->setFlash('error', Yii::t('booking', 'Insuffisient funds.'));
                return $this->renderAjax(false, 'OK');
            }

            if ($model->validate()) {
                $transaction = Payment::createInternalTransaction($model->total_cost, $nonadv, $adv->user, Payment::REASON_BOOKING, true);
                $model->transaction_id = $transaction->id;
                $model->save(false);
                
                if ($model->secure_code) {
                    Yii::$app->session->setFlash('success', Yii::t('booking', 'Booking successful, please wait for confirmation. Secure Code: {code}', [
                        'code' => $model->secure_code,
                    ]));
                } else {
                    Yii::$app->session->setFlash('success', Yii::t('booking', 'Booking successful, please wait for confirmation.'));
                }

                \common\helpers\Email::send(
                $model->advertiser->user->email, 'booking_adv', [
                    'subject' => \Yii::t('app', 'New booking request'),
                    'model' => $model,
                ]);
                
                return $this->renderAjax(true);
            } else {
                Yii::$app->session->setFlash('error', $model->getFirstErrorMessage());
                return $this->renderAjax(false, 'OK');
            }
        } else {
            if ($model->total_cost > $nonadv->balance) {
                return $this->renderAjax(false, Yii::t('booking', 'Insuffisient funds. Required {cost}, you have {balance}, {needed} needed. Please refill.', [
                    'cost' => Toolbox::formatMoney($model->total_cost),
                    'balance' => Toolbox::formatMoney($nonadv->balance),
                    'needed' => Toolbox::formatMoney($model->total_cost - $nonadv->balance),
                ]));
            } else {
                $content = $this->controller->renderAjax('book', ['adv' => $adv, 'nonadv' => $nonadv, 'model' => $model]);
                return $this->renderAjax(true, $content);
            }
        }
    }

}


