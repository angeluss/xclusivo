<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<style>
.go-button{display:inline-block;padding:6px 12px;font-size:21px;  text-align:center;border:1px solid transparent;border-radius:4px;text-decoration:none;color: #ffffff;line-height: 41px;height: 43px;background: #61c202;}
</style>
<div class="password-reset">
  <p>Hi <?= $user->username ?>,</p>

  <p>We received a request to reset your password for your Eventbrite account: <?=Html::a($user->email, ['mailto:'.$user->email]);?>. We're here to help!</p>

  <p>Simple click on the button to set a new password:</p>

  <p><a href="<?=$resetLink;?>" class=".go-button" style="display:inline-block;padding:6px 12px;font-size:21px;text-align:center;border:1px solid transparent;border-radius:4px;text-decoration:none;color: #ffffff;line-height:41px;height: 43px;background:#61c202;">Set a New Password</a></p>

  <p>If you didn't ask to change your password, don't worry! Your password is still safe and you can delete this email.</p>

  <p>Cheers,</p>
  <p>Eventbrite</p>

</div>
