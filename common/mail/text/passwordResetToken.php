<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
Hi <?= $user->username ?>,

We received a request to reset your password for your Eventbrite account: <?=$user->email;?>. We're here to help!

Simple click on the link to set a new password:

<?=$resetLink;?>

If you didn't ask to change your password, don't worry! Your password is still safe and you can delete this email.

Cheers,
Eventbrite
