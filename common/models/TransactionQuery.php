<?php


namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use common\models\User;
use yii\base\Object;
use frontend\models\Advertiser;


class TransactionQuery extends ActiveQuery
{

    public function byUser(User $user)
    {
        if ($user->isNewRecord || !$user->primaryKey) {
            throw new \yii\base\Exception('User model is not saved yet.');
        }
        
        $this->andWhere(['or', ['sender_id' => $user->id], ['receiver_id' => $user->id]]);
        return $this;
    }
}
