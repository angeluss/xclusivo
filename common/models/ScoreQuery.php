<?php


namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use common\helpers\Scoring;
use common\models\User;
use yii\base\Object;
use frontend\models\Advertiser;


class ScoreQuery extends ActiveQuery
{


	/**
	 * 
	 * @return \common\models\ScoreQuery
	 */
	public function expired()
	{
		$this->andWhere(['<', 'created_at', time() - Scoring::expirationLimit()]);
		return $this;
	}


	/**
	 * 
	 * @return \common\models\ScoreQuery
	 */
	public function active()
	{
		$this->andWhere(['>', 'valid_until', time()]);
		return $this;
	}


	/**
	 * 
	 * @return \common\models\ScoreQuery
	 */
	public function newest()
	{
		return $this->orderBy('valid_until DESC');
	}


	/**
	 * 
	 * @return \common\models\ScoreQuery
	 */
	public function notUpdatedToday()
	{
		$dayStart = strtotime("midnight", time());
		$this->andWhere(['<', '{{%scores}}.updated_at', $dayStart]);
		return $this;
	}


	/**
	 * 
	 * @return \common\models\ScoreQuery
	 */
	public function withPausedProfiles()
	{
		$this->joinWith(['advertiser.user'])->andWhere(['user.status' => User::STATUS_PAUSE]);
		return $this;
	}


	/**
	 * 
	 * @return \common\models\ScoreQuery
	 */
	public function byCustomId($value)
	{
		return $this->andWhere(['custom_id' => $value]);
	}


	/**
	 * 
	 * @return \common\models\ScoreQuery
	 */
	public function byEntity(ActiveRecord $r)
	{
		return $this->andWhere(['entity_id' => $r->primaryKey])->andWhere(['entity_class' => $r->className()]);
	}


	/**
	 * 
	 * @return \common\models\ScoreQuery
	 */
	public function byClass($something)
	{
		$className = ($something instanceof Object) ? get_class($something) : (string) $something;
		return $this->andWhere(['entity_class' => $className]);
	}


	/**
	 * 
	 * @return \common\models\ScoreQuery
	 */
	public function byAdvertiser($adv)
	{
		$advId = ($adv instanceof Advertiser) ? $adv->id : intval($adv);
		return $this->andWhere(['advertiser_id' => $advId]);
	}


	/**
	 * 
	 * @return \common\models\ScoreQuery
	 */
	public function byUser($user = false)
	{
		if ($user !== false) {
			$userId = ($user instanceof User) ? $user->id : $user;
			return $this->andWhere(['user_id' => $user]);
		} else {
			return $this->andWhere(['not', ['user_id' => null]]);
		}
	}


	/**
	 * 
	 * @return \common\models\ScoreQuery
	 */
	public function bySystem()
	{
		return $this->byUser(null);
	}


}
