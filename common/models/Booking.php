<?php

namespace common\models;


use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use frontend\models\Advertiser;
use common\models\User;
use common\helpers\Toolbox;
use common\helpers\Payment;


/**
 * This is the model class for table "bookings".
 *
 * @property integer $id
 * @property integer $advertiser_id
 * @property integer $user_id
 * @property integer $status
 * @property string $from_date
 * @property string $to_date
 * @property string $created_at
 * @property string $updated_at
 * @property float $total_cost
 * @property boolean $is_secure
 * @property string $secure_code
 * @property boolean $no_show_guarantee 
 * @property integer $transaction_id
 * 
 * @property int $hoursBooked
 *
 * @property Advertiser $advertiser
 * @property User $user
 * @property Transaction $transaction
 */
class Booking extends \yii\db\ActiveRecord
{

    const STATUS_PENDING = 'Pending';
    const STATUS_APPROVED = 'Approved';
    const STATUS_STUB = 'Stub';



    public $ok;



    /**
     * 
     * @return \common\models\BookingQuery
     */
    public static function find()
    {
        return new BookingQuery(get_called_class());
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bookings}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_date', 'to_date', 'is_secure', 'no_show_guarantee', 'ok'], 'safe'],
            [['advertiser_id', 'status', 'from_date', 'to_date',], 'required'],
            [['total_cost'], 'required', 'when' => function($model) {
                    $model->status != self::STATUS_STUB;
                }],
            [['advertiser_id', 'user_id', 'transaction_id', 'from_date', 'to_date'], 'integer'],
            [['from_date', 'to_date'], 'filter', 'filter' => function ($value) {
                    return Toolbox::ensureTimestamp($value);
                }],
//			[['advertiser_id'], 'exist', 'skipOnError' => true, 'targetClass' => Advertiser::className(), 'targetAttribute' => ['advertiser_id' => 'id']],
            [['advertiser_id'], 'exist', 'skipOnError' => true, 'targetClass' => Advertiser::className(), 'targetAttribute' => 'id'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => 'id'],
        //[['transaction_id'], 'exist', 'skipOnError' => true, 'targetClass' => Transaction::className(), 'targetAttribute' => ['transaction_id' => 'id']],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
        ];
    }


    public function behaviors()
    {
        return [
            \common\behaviors\ExtractErrorBehavior::className(),
            \common\behaviors\CaptureRequestBehavior::className(),
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertiser()
    {
        return $this->hasOne(Advertiser::className(), ['id' => 'advertiser_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['id' => 'transaction_id']);
    }


    public function getFeedId()
    {
        return 'b' . $this->id . 'u' . $this->user_id . 'a' . $this->advertiser_id;
    }


    /**
     * 
     * @return string
     */
    public function getStatusClasses()
    {
        $suffix = strtolower($this->status);
        $result = 'js-status-' . $suffix . ' ' . 'fc-status-' . $suffix;
        return $result;
    }


    public function approve()
    {
        $this->status = self::STATUS_APPROVED;
        $this->save(false, ['status', 'no_show_guarantee']);
        // TODO: other actions like emailing or messaging
    }


    /**
     * Decline a booking by advertiser
     */
    public function decline()
    {
        if ($this->isNewRecord) {
            throw new \yii\base\Exception('Booking record is not saved yet.');
        }

        if (!$this->status == self::STATUS_PENDING) {
            throw new \yii\base\Exception('Unable to decline non-pending booking.');
        }

        if ($this->transaction_id && $this->transaction) {
            $this->transaction->cancel()->delete();
        }

        $this->delete();
        // TODO: other actions like emailing or messaging
    }


    /**
     * Cancelling booking by non-advertiser or by system (unpaid expired booking) with $noPendingCheck == true
     * 
     * @param boolean $noPendingCheck
     * @throws \yii\base\Exception
     */
    public function cancel($noPendingCheck = false)
    {
        if ($this->isNewRecord) {
            throw new \yii\base\Exception('Booking record is not saved yet.');
        }

        if (!$noPendingCheck && !$this->status == self::STATUS_PENDING) {
            throw new \yii\base\Exception('Unable to cancel non-pending booking.');
        }

        if ($this->transaction_id && $this->transaction) {
            $this->transaction->cancel()->delete();
        }

        $this->delete();
        // TODO: other actions like emailing or messaging
    }


    public function commit()
    {
        if ($this->isNewRecord) {
            throw new \yii\base\Exception('Booking record is not saved yet.');
        }

        if (!$this->status == self::STATUS_APPROVED) {
            throw new \yii\base\Exception('Unable to process unapproved booking.');
        }

        if ($this->transaction_id && $this->transaction) {
            $this->transaction->commit();
        }

        if ($this->secure_code) {
            $this->secure_code = null;
            $this->save(false, ['secure_code']);
        }
    }


    public function guarantee()
    {
        if ($this->isNewRecord) {
            throw new \yii\base\Exception('Booking record is not saved yet.');
        }

        if (!$this->status == self::STATUS_APPROVED) {
            throw new \yii\base\Exception('Unable to process unapproved booking.');
        }

        if ($this->transaction_id && $this->transaction) {
            $fine = Payment::noShowGuaranteePenalty($this->transaction->amount);
            $this->transaction->cancel();
            $penalty = Payment::createInternalTransaction($fine, $this->transaction->sender, $this->transaction->receiver, Payment::REASON_NO_SHOW);
            $penalty->commit();
            $this->transaction->delete();
            $this->transaction_id = $penalty->id;
            $this->secure_code = null;
            $this->save(false, ['transaction_id' . 'secure_code']);
        }
    }


    /**
     * 
     * @return boolean
     */
    public function getIsExpired()
    {
        $expiration = time() + \common\helpers\Calendar::eventExpirationLimit() * 60 * 60;
        return $expiration > $this->from_date;
    }


    /**
     * 
     * @return int
     */
    public function getHoursBooked()
    {
        return ($this->to_date - $this->from_date) / (60 * 60);
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->is_secure) {
                    $this->secure_code = $this->generateSecureCode();
                }

                if (empty($this->transaction_id)) {
                    $adv = Advertiser::find()->joinWith(['user'])->where([Advertiser::tableName() . '.id' => $this->advertiser_id])->one();
                    $nonadv = User::find()->where(['id' => $this->user_id])->one();
                    $transaction = Payment::createInternalTransaction($this->total_cost, $nonadv, $adv->user, Payment::REASON_BOOKING, true);
                    $this->transaction_id = $transaction->id;
                }
            }
        }

        return true;
    }


    /**
     * @return string
     */
    protected function generateSecureCode()
    {
        if (!$this->advertiser_id) {
            throw new \yii\base\Exception('Advertiser ID required');
        }

        if (!$this->user_id) {
            throw new \yii\base\Exception('Non-advertiser user ID required');
        }

        do {
            $result = md5($this->advertiser_id . $this->user_id . uniqid('', true));
            $result = strtoupper(substr($result, 0, 10));
        } while (Booking::find()->where(['secure_code' => $result])->count());

        return $result;
    }

}


