<?php

namespace common\models;


use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use frontend\models\Advertiser;
use common\models\User;
use common\models\Refund;
use common\helpers\Toolbox;


/**
 * This is the model class for table "transactions".
 *
 * @property integer $id
 * @property string $external_id
 * @property integer $sender_id
 * @property integer $receiver_id
 * @property string $status
 * @property string $sender_email
 * @property string $receiver_email
 * @property float $amount 
 * @property string $gateway
 * @property string $reason
 * @property string $comment
 * @property string $created_at
 * @property string $updated_at
 *
 * @property boolean $canBeReverted
 * 
 * @property User $sender
 * @property User $receiver
 */
class Transaction extends \yii\db\ActiveRecord
{

    const STATUS_PENDING = 'Pending';
    const STATUS_COMPLETED = 'Completed';
    const STATUS_CANCELLED = 'Cancelled';
    const STATUS_REVERTED = 'Reverted';
    const GATEWAY_SKRILL = 'Skrill';
    const GATEWAY_STRIPE = 'Stripe';
    const GATEWAY_MANUAL_PAYOUT = 'ManualPayout';



    /**
     * 
     * @return \common\models\BookingQuery
     */
    public static function find()
    {
        return new TransactionQuery(get_called_class());
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transactions}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'reason', 'status'], 'required'],
            [['receiver_id', 'sender_id'], 'integer', 'skipOnEmpty' => true],
            [['sender_id', 'sender_email'], 'required', 'when' => function($model) {
                    return empty($model->receiver_id) || $model->gateway == self::GATEWAY_MANUAL_PAYOUT || empty($model->gateway);
                }],
            [['receiver_id', 'receiver_email'], 'required', 'when' => function($model) {
                    return empty($model->sender_id) || ($model->gateway && $model->gateway != self::GATEWAY_MANUAL_PAYOUT) || empty($model->gateway);
                }],
            [['sender_id', 'sender_email'], 'filter', 'filter' => function($value) {
                    return null;
                }, 'when' => function($model) {
                    return !empty($model->receiver_id) && (!empty($model->gateway) && $model->gateway != self::GATEWAY_MANUAL_PAYOUT);
                }],
            [['receiver_id', 'receiver_email'], 'filter', 'filter' => function($value) {
                    return null;
                }, 'when' => function($model) {
                    return !empty($model->sender_id) && ($model->gateway == self::GATEWAY_MANUAL_PAYOUT);
                }],
            [['status'], 'in', 'range' => [self::STATUS_PENDING, self::STATUS_COMPLETED, self::STATUS_CANCELLED, self::STATUS_REVERTED]],
            [['comment'], 'string', 'max' => 2000, 'skipOnEmpty' => true],
            [['comment'], 'filter', 'filter' => 'strip_tags'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
        ];
    }


    public function behaviors()
    {
        return [
            \common\behaviors\ExtractErrorBehavior::className(),
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(User::className(), ['id' => 'sender_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiver()
    {
        return $this->hasOne(User::className(), ['id' => 'receiver_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooking()
    {
        return $this->hasOne(Booking::className(), ['transaction_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefund()
    {
        return $this->hasOne(Refund::className(), ['transaction_id' => 'id']);
    }


    /**
     * 
     * @return \common\models\Transaction
     * @throws \yii\base\Exception
     */
    public function commit()
    {
        if ($this->status != self::STATUS_PENDING) {
            throw new \yii\base\Exception('Transaction is not in pending state.');
        }

        if ($this->isNewRecord) {
            throw new \yii\base\Exception('Transaction is not saved yet.');
        }

        if ($this->receiver_id && $this->receiver) {
            $this->receiver->balancePlus($this->amount);
        }

        $this->saveStatus(self::STATUS_COMPLETED);
        return $this;
    }


    /**
     * 
     * @return \common\models\Transaction
     * @throws \yii\base\Exception
     */
    public function cancel()
    {
        if ($this->status != self::STATUS_PENDING) {
            throw new \yii\base\Exception('Transaction is not in pending state.');
        }

        if ($this->isNewRecord) {
            throw new \yii\base\Exception('Transaction is not saved yet.');
        }

        if ($this->sender_id && $this->sender) {
            $this->sender->balancePlus($this->amount);
        }

        $this->saveStatus(self::STATUS_CANCELLED);
        return $this;
    }


    /**
     * 
     * @return \common\models\Transaction
     * @throws \yii\base\Exception
     */
    public function revert()
    {
        if ($this->status != self::STATUS_COMPLETED) {
            throw new \yii\base\Exception('Transaction is not in completed state.');
        }

        if ($this->isNewRecord) {
            throw new \yii\base\Exception('Transaction is not saved yet.');
        }

        if ($this->sender_id && $this->sender) {
            $this->sender->balancePlus($this->amount);
        }

        if ($this->receiver_id && $this->receiver) {
            $this->receiver->balanceMinus($this->amount);
        }

        $this->saveStatus(self::STATUS_REVERTED);
        return $this;
    }


    /**
     * 
     * @return boolean
     */
    public function getCanBeReverted()
    {
        if ($this->receiver_id && $this->receiver) {
            return $this->receiver->balance > $this->amount;
        } else {
            return true;
        }
    }


    /**
     * 
     * @return boolean
     */
    public function getIsInternal()
    {
        return $this->sender_id && $this->receiver_id && !$this->gateway;
    }


    public function beforeValidate()
    {
        if ($this->isNewRecord && !$this->status) {
            $this->status = self::STATUS_PENDING;
        }

        return parent::beforeValidate();
    }


    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            if ($this->sender_id && $this->status == self::STATUS_PENDING) {
                $sender = User::find()->where(['id' => $this->sender_id])->one();
                $sender->balanceMinus($this->amount);
            }
        }

        return parent::afterSave($insert, $changedAttributes);
    }


    /**
     * 
     * @param string $status
     * @return \common\models\Transaction
     * @throws \yii\base\Exception
     */
    protected function saveStatus($status)
    {
        $this->status = $status;

        if (!$this->save(true)) {
            throw new \yii\base\Exception($this->getFirstErrorMessage());
        }

        return $this;
    }

}


