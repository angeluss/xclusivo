<?php


namespace common\models;

use yii\db\ActiveQuery;
use common\helpers\Calendar;


class BookingQuery extends ActiveQuery
{


	public function nonAdvertiserCalendar($nonAdvId, $mainAdvId)
	{
		return $this->andWhere([
				'or',
				['user_id' => $nonAdvId],
				['advertiser_id' => $mainAdvId],
		]);
	}


	/**
	 * 
	 * @return BookingQuery
	 */
	public function future()
	{
		return $this->andWhere(['>', 'from_date', time() + \common\helpers\Calendar::eventExpirationLimit() * 60 * 60]);
	}


	/**
	 * 
	 * @return BookingQuery
	 */
	public function past()
	{
		return $this->andWhere(['<=', 'from_date', time() + \common\helpers\Calendar::eventExpirationLimit() * 60 * 60]);
	}


	/**
	 * 
	 * @return BookingQuery
	 */
	public function pastInHours($hours, $useEndDate = true)
	{
		return $this->andWhere(['<=', $useEndDate ? 'to_date' : 'from_date', time() - ($hours * 60 * 60)]);
	}


	public function notPaid()
	{
		return $this
				->joinWith(['transaction'])
				->andWhere([Transaction::tableName() . '.status' => Transaction::STATUS_PENDING]);
	}


	/**
	 * 
	 * @return BookingQuery
	 */
	public function approved()
	{
		return $this->andWhere([Booking::tableName() . '.status' => Booking::STATUS_APPROVED]);
	}


	/**
	 * 
	 * @return BookingQuery
	 */
	public function active()
	{
		return $this->future()->notStub()->approved();
	}


	/**
	 * 
	 * @return BookingQuery
	 */
	public function pending()
	{
		return $this->future()->notStub()->andWhere(['status' => Booking::STATUS_PENDING]);
	}


	/**
	 * 
	 * @return BookingQuery
	 */
	public function notStub()
	{
		return $this->andWhere(['<>', 'status', Booking::STATUS_STUB]);
	}


	/**
	 * 
	 * @return BookingQuery
	 */
	public function secured()
	{
		return $this->andWhere(['is_secure' => 1]);
	}


	/**
	 * 
	 * @return BookingQuery
	 */
	public function noShowGuaranteed()
	{
		return $this->andWhere(['no_show_guarantee' => 1]);
	}


	/**
	 * 
	 * @return BookingQuery
	 */
	public function notAnswered()
	{
		return $this->pending()->andWhere(['<', 'created_at', time() - Calendar::advertiserReactionLimit() * 60 * 60]);
	}


}
