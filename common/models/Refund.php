<?php

namespace common\models;


use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use frontend\models\Advertiser;
use common\models\User;
use common\models\Transaction;
use common\helpers\Toolbox;
use common\helpers\Payment;


/**
 * This is the model class for table "transactions".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $user_email
 * @property float $amount
 * @property string $description
 * @property string $admin_comment
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $sort_order
 *
 * @property boolean $canBeReverted
 *
 * @property \common\models\User $user
 * @property \common\models\Transaction $transaction
 */
class Refund extends \yii\db\ActiveRecord
{

    const STATUS_PENDING = 'Pending';
    const STATUS_COMPLETED = 'Completed';
    const STATUS_CANCELLED = 'Cancelled';



    public $balanceCheck = 0;



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%refunds}}';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'user_id', 'user_email', 'description'], 'required'],
            [['amount', 'user_id', 'user_email', 'description', 'admin_comment'], 'safe'],
            [['amount'], 'number', 'min' => Payment::minRefund()],
            [['user_id', 'transaction_id'], 'integer', 'min' => 1],
            [['admin_comment', 'description'], 'string', 'max' => 2000, 'skipOnEmpty' => true],
            [['admin_comment', 'description'], 'filter', 'filter' => 'strip_tags'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => 'id'],
            [['amount'], 'validateAmount'],
            [['sort_order'], 'number']
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
        ];
    }


    public function behaviors()
    {
        return [
            \common\behaviors\ExtractErrorBehavior::className(),
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['id' => 'transaction_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBooking()
    {
        return $this->hasOne(Booking::className(), ['transaction_id' => 'id']);
    }


    /**
     *
     * @return \common\models\Refund
     * @throws \yii\base\Exception
     */
    public function done()
    {
        if ($this->isNewRecord) {
            throw new \yii\base\Exception('Refund is not saved yet.');
        }

        if ($this->status != self::STATUS_PENDING) {
            throw new \yii\base\Exception('Refund already processed.');
        }

        if ($this->transaction) {
            $this->transaction->commit();
        }

        $this->saveStatus(self::STATUS_COMPLETED);
        return $this;
    }


    /**
     *
     * @return \common\models\Refund
     * @throws \yii\base\Exception
     */
    public function cancel()
    {
        if ($this->isNewRecord) {
            throw new \yii\base\Exception('Refund is not saved yet.');
        }

        if ($this->status != self::STATUS_PENDING) {
            throw new \yii\base\Exception('Refund already processed.');
        }

        if ($this->transaction) {
            $this->transaction->cancel();
        }

        $this->saveStatus(self::STATUS_CANCELLED);
        return $this;
    }


    public function beforeValidate()
    {
        if ($this->isNewRecord && !$this->status) {
            $this->status = self::STATUS_PENDING;
        }

        return parent::beforeValidate();
    }


    public function afterSave($insert, $changedAttributes)
    {
        return parent::afterSave($insert, $changedAttributes);
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if (empty($this->transaction_id)) {
                    $user = $this->getUser()->one();
                    $transaction = Payment::createRefundTransaction($this->amount, $user);
                    $this->transaction_id = $transaction->id;
                }
            }
        }
        $this->sort_order = ($this->status == self::STATUS_PENDING ? 1 : 0);

        return true;
    }

    public function isPending()
    {
      return $this->status == self::STATUS_PENDING;
    }

    public function validateAmount($attribute, $params)
    {
        if (!$this->balanceCheck) {
            $user = $this->getUser()->one();

            if (!$user) {
                throw new \yii\base\Exception('User not found.');
            }

            $this->balanceCheck = $user->balance;
        }

        if ($this->$attribute > $this->balanceCheck) {
            $this->addError($attribute, Yii::t('app', 'Insuffisient funds.'));
        }
    }


    /**
     *
     * @param string $status
     * @return \common\models\Transaction
     * @throws \yii\base\Exception
     */
    protected function saveStatus($status)
    {
        $this->status = $status;

        if (!$this->save(true)) {
            throw new \yii\base\Exception($this->getFirstErrorMessage());
        }

        return $this;
    }

}
