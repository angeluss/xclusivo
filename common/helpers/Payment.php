<?php

namespace common\helpers;


use Yii;
use common\models\User;
use common\models\Transaction;
use common\models\TransactionQuery;


class Payment
{

    const REASON_BOOKING = 'Booking Appointment';
    const REASON_NO_SHOW = 'No Show Guarantee Fine';
    const REASON_REFILL = 'Balance Refilling';
    const REASON_REFUND = 'Refund Request';



    /**
     * 
     * @return int
     */
    public static function noShowGuaranteePenaltyPercent()
    {
        return 25;
    }

    
    public static function bookingFeePercent()
    {
        return 4;
    }

    
    /**
     * 
     * @param float $amount
     * @return float
     */
    public static function noShowGuaranteePenalty($amount)
    {
        $part = self::noShowGuaranteePenaltyPercent() / 100;
        return self::round($amount * $part);
    }


    /**
     * 
     * @return int Hours
     */
    public static function unpaidBookingTimeout()
    {
        return 24;
    }


    public static function minRefund()
    {
        return 1.00;
    }

    public static function minRefill()
    {
        return 1.00;
    }

    /**
     * 
     * @param float $amount
     * @return float
     */
    public static function round($amount)
    {
        return round($amount, 2);
    }


    /**
     * 
     * @param float $amount
     * @param User $sender
     * @param User $receiver
     * @param string $reason
     * @param boolean $saveNow
     * @return \common\models\Transaction
     */
    public static function createInternalTransaction($amount, User $sender, User $receiver, $reason, $saveNow = true)
    {
        $data = [
            'sender_id' => $sender->id,
            'receiver_id' => $receiver->id,
            'reason' => $reason,
            'sender_email' => $sender->email,
            'receiver_email' => $receiver->email,
            'amount' => $amount,
        ];

        return self::createTransaction($data, $saveNow);
    }


    /**
     * 
     * @param float $amount
     * @param User $receiver
     * @param string $externalId
     * @param string $gateway
     * @param boolean $saveNow
     * @return \common\models\Transaction
     */
    public static function createRefillTransaction($amount, User $receiver, $externalId, $gateway, $saveNow = true)
    {
        $data = [
            'receiver_id' => $receiver->id,
            'reason' => self::REASON_REFILL,
            'receiver_email' => $receiver->email,
            'amount' => $amount,
            'gateway' => $gateway,
            'external_id' => $externalId,
        ];

        return self::createTransaction($data, $saveNow);
    }


    /**
     * 
     * @param float $amount
     * @param User $receiver
     * @param string $externalId
     * @param string $gateway
     * @param boolean $saveNow
     * @return \common\models\Transaction
     */
    public static function createRefundTransaction($amount, User $sender, $saveNow = true)
    {
        $data = [
            'sender_id' => $sender->id,
            'gateway' => Transaction::GATEWAY_MANUAL_PAYOUT,
            'reason' => self::REASON_REFUND,
            'sender_email' => $sender->email,
            'amount' => $amount,
        ];

        return self::createTransaction($data, $saveNow);
    }


    /**
     * 
     * @param array $attributes
     * @param boolean $saveNow
     * @return \common\models\Transaction
     * @throws \yii\base\Exception
     */
    protected static function createTransaction($attributes, $saveNow = true)
    {
        $model = new Transaction();
        $model->setAttributes($attributes, false);

        if ($saveNow && !$model->save()) {
            throw new \yii\base\Exception($model->getFirstErrorMessage());
        }

        return $model;
    }

}


