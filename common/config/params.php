<?php
return [
    'adminEmail' => 'info@xclusivo.org',
    'supportEmail' => 'info@xclusivo.org',
    'user.passwordResetTokenExpire' => 3600,
];
