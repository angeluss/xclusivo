<?php

namespace common\components\stripe;


use Yii;
use yii\base\Component;


//use Stripe\Stripe as StripeBase;

class Stripe extends Component
{

    public $secretKey;
    public $publicKey;
    public $silentErrors = false;



    public function init()
    {
        parent::init();
        \Stripe\Stripe::setApiKey($this->secretKey);
    }


    public function retrieveCustomer($stripeId, $params = [])
    {
        $params = ['id' => $stripeId];
        return $this->call('Customer', 'retrieve', $params);
    }

    /*
    public function updateCustomer($stripeId, $params = [])
    {
        $params = ['id' => $stripeId];
        
        if ($source) {
            $params['source'] = $source;
        }
        
        return $this->call('Customer', 'retrieve', $params);
    }
     * 
     */

    public function createCustomer($email, $source = null, $params = [])
    {
        $params['email'] = $email;

        if ($source) {
            $params['source'] = $source;
        }

        return $this->call('Customer', 'create', $params);
    }


    public function createCharge($stripeCustomerId, $amount, $currency, $source = null, $params = [])
    {
        $params['customer'] = $stripeCustomerId;
        $params['amount'] = $amount;
        $params['currency'] = $currency;

        if ($source) {
            $params['source'] = $source;
        }

        return $this->call('Charge', 'create', $params);
    }


    protected function call($entity, $method, $params = [])
    {
        $className = '\Stripe\\' . $entity;
        return $className::$method($params);
    }

}


