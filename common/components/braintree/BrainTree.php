<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace common\components\braintree;

use yii\base\Component;
use Braintree\Configuration as Braintree_Configuration;
use Braintree\ClientToken as Braintree_ClientToken;
use yii\web\View;


class BrainTree extends Component
{

    const ENV_SANDBOX = 'sandbox';
    const TEST_NONCE = 'fake-valid-nonce';

    public $environment = self::ENV_SANDBOX;
    public $merchantId;
    public $publicKey;
    public $privateKey;
    public $testMode = false;
    protected $_errors = false;


    public function init()
    {
        parent::init();
        $this->configure('environment', $this->environment);
        $this->configure('merchantId', $this->merchantId);
        $this->configure('publicKey', $this->publicKey);
        $this->configure('privateKey', $this->privateKey);
        /*
          Braintree_Configuration::environment($this->environment);
          Braintree_Configuration::merchantId($this->merchantId);
          Braintree_Configuration::publicKey($this->publicKey);
          Braintree_Configuration::privateKey($this->privateKey);
         * 
         */
    }


    public function getClientToken()
    {
        return $this->exec('ClientToken', 'generate');
    }


    public function getErrors()
    {
        return $this->_errors;
    }


    public function hasErrors()
    {
        return (boolean) $this->_errors;
    }


    /**
     * 
     * @return array
     */
    public function getTransactionValidStatuses()
    {
        return [
            \Braintree\Transaction::AUTHORIZED,
            \Braintree\Transaction::AUTHORIZING,
            \Braintree\Transaction::SETTLED,
            \Braintree\Transaction::SETTLING,
            \Braintree\Transaction::SETTLEMENT_CONFIRMED,
            \Braintree\Transaction::SETTLEMENT_PENDING,
            \Braintree\Transaction::SUBMITTED_FOR_SETTLEMENT
        ];
    }


    public function isStatusValid($status)
    {
        return in_array($status, $this->getTransactionValidStatuses());
    }


    public function doTransactionSale($amount, $paymentMethodNonce, $options = [])
    {
        // Prevent using real data in test mode (fake-... nonces should be used)
        if ($this->testMode && strpos($paymentMethodNonce, 'fake-') !== 0) {
            $paymentMethodNonce = 'fake-valid-nonce';
        }

        $result = $this->exec('Transaction', 'sale', [
            'amount' => $amount,
            'paymentMethodNonce' => $paymentMethodNonce,
            'options' => $options,
        ]);
        return $this->processResult($result, 'transaction');
    }


    public function jsRegister($view)
    {
        BrainTreeAsset::register($view);
    }


    public function jsInitForm($view, $formId, $container = 'bt-dropin')
    {
        $formId = str_replace('#', '', $formId);
        $view->registerJs($this->jsRenderDropInCode($view, $formId, $container, $this->getClientToken()), View::POS_END);
    }


    protected function configure($name, $value)
    {
        $this->exec('Configuration', $name, (string) $value);
    }


    protected function exec($class, $method, $params = null, $prefix = 'Braintree')
    {
        $className = $prefix . '\\' . $class;

        if (is_array($params) || is_string($params)) {
            return $className::$method($params);
        } else {
            return $className::$method();
        }
    }


    protected function processResult($result, $returnKey = null)
    {
        $fail = !((boolean) $result->success);

        if ($returnKey && !$fail) {
            $fail = is_null($result->$returnKey);
        }

        if ($fail) {
            $this->_errors = $result->errors->deepAll();
            return false;
        } else {
            $this->_errors = null;
            return ($returnKey) ? $result->$returnKey : $result;
        }
    }


    protected function jsRenderDropInCode($view, $formId, $container, $token = null)
    {
//        $fileName = ($this->testMode ? 'demo_' : 'live_').'dropin_js.php';
        $fileName = 'dropin_js.php';

        if (!$token) {
            $token = $this->getClientToken();
        }

        return $view->renderFile(__DIR__ . '/views/' . $fileName, [
                'formId' => $formId,
                'container' => $container,
                'token' => $token,
        ]);
    }


    /*
      Braintree_Configuration::environment('sandbox');
      Braintree_Configuration::merchantId('use_your_merchant_id');
      Braintree_Configuration::publicKey('use_your_public_key');
      Braintree_Configuration::privateKey('use_your_private_key');
     */

}
