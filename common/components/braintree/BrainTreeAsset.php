<?php


namespace common\components\braintree;

use yii\web\AssetBundle;


class BrainTreeAsset extends AssetBundle
{

    public $sourcePath = null;
    public $js = [
        'https://js.braintreegateway.com/js/braintree-2.27.0.min.js',
    ];

}
