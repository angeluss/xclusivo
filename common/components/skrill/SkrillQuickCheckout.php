<?php
/*
 * Test Cards:
 * Mastercard 5438311234567890
 * Visa 4000001234567890
 * Amex 371234500012340
 * 
 * Test Emails:
 * demoqco@sun-fish.com			- Fixed Payment Options (Fixed Split Gateway)
 * demoqcoflexible@sun-fish.com	- Flexible Payment Options (Flexible Split Gateway)
 * demoqcofixedhh@sun-fish.com	- Fixed Payment Options (Fixed Split Gateway) with Reduced header option enabled.
 */


namespace common\components\skrill;

use Yii;
use yii\base\Component;


class SkrillQuickCheckout extends Component
{

	const LIVE_GATEWAY = 'https://pay.skrill.com/';
	const TEST_GATEWAY = 'https://www.skrill.com/app/test_payment.pl';
	const LANGUAGES = 'BG,CS,DA,DE,EL,EN,ES,FI,FR,IT,ZH,NL,PL,RO,RU,SV,TR,JA';
	const CURRENCIES = 'EUR,USD,GBP,HKD,'
		. 'SGD,JPY,CAD,AUD,CHF,DKK,DKK,'
		. 'SEK,NOK,ILS,MYR,NZD,NZD,TRY,'
		. 'AED,MAD,QAR,SAR,TWD,THB,CZK,'
		. 'HUF,BGN,PLN,ISK,INR,KRW,ZAR,'
		. 'RON,HRK,JOD,OMR,RSD,TND,BHD,'
		. 'KWD';
	const MERCHANT_FIELDS_MAX_LENGTH = 240;
	const TEST_EMAILS = 'demoqco@sun-fish.com,demoqcoflexible@sun-fish.com,demoqcofixedhh@sun-fish.com';

	public $payToEmail = 'demoqco@sun-fish.com';
	public $formDefaults = [];
	public $testMode = false;


	public function init()
	{
		parent::init();
		// Protection against real payments at testing gateway
		if ($this->testMode && !in_array($this->payToEmail, explode(',', self::TEST_EMAILS))) {
			$this->payToEmail = 'demoqcoflexible@sun-fish.com';
		}


		//if (isset($this->formDefaults['']))
	}


	/**
	 * 
	 * @param array $attributes
	 * @return SkrillQuickCheckoutForm
	 */
	public function createForm($attributes = [])
	{
		$params = array_merge($this->formDefaults, $attributes);
		unset($params['component']);
		$params['pay_to_email'] = $this->payToEmail;
		$form = Yii::createObject(SkrillQuickCheckoutForm::className(), ['component' => $this]);

		foreach ($params as $key => $value) {
			$form->$key = $value;
		}

		return $form;
	}


	public function getSessionId($data)
	{
		if ($data instanceof SkrillQuickCheckoutForm) {
			$data = $data->getAttributesWithValues();
		}
		unset($data['_csrf']);
		$data['prepare_only'] = 1;
		$response = $this->request($data);
	}


	/**
	 * @return string 
	 */
	public function getActionUrl()
	{
		return ($this->testMode) ? self::TEST_GATEWAY : self::LIVE_GATEWAY;
	}


	protected function request($params)
	{
		$url = $this->getActionUrl();
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3');
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));   
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		$result = curl_exec($ch);
		curl_close($ch);
		print_r($params);
		var_dump($result); die();
		if ($result) {
			$result = json_decode($result, true);
            
            
			return $result;
		} else {
			return false;
		}
	}


}
