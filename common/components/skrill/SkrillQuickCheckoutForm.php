<?php


namespace common\components\skrill;

use Yii;
use yii\base\Model;


class SkrillQuickCheckoutForm extends Model
{

	public $sid;
	public $pay_to_email;
	public $recipient_description;
	public $transaction_id;
	public $return_url;
	public $return_url_text = 'Return to main website';
	public $return_url_target = '_top';
	public $cancel_url;
	public $cancel_url_target = '_top';
	public $status_url;
	public $status_url2;
	public $language;
	public $logo_url;
	public $prepare_only = 0;
	public $dynamic_descriptor;
	public $rid;
	public $ext_ref_id;
	public $merchant_fields = [];
	public $amount;
	public $currency = 'EUR';
	protected $_component = null;


	public function rules()
	{
		return [
			[['pay_to_email', 'amount'], 'required', 'when' => function($model) {
				return empty($model->sid);
			}],
			[['return_url', 'cancel_url', 'status_url', 'status_url2'], 'filter', 'filter' => function($value) {
				return ($value) ? \yii\helpers\Url::to($value, true) : $value;
			}],
			[['logo_url'], 'filter', 'filter' => function($value) {
				return ($value) ? \yii\helpers\Url::to($value, 'https') : $value;
			}],
			[['pay_to_email'], 'string', 'max' => 50],
			[['pay_to_email'], 'email'],
			[['recipient_description'], 'string', 'max' => 30],
			[['transaction_id', 'rid', 'ext_ref_id'], 'string', 'max' => 100],
			[['return_url', 'cancel_url', 'logo_url'], 'url'],
			[['return_url', 'cancel_url', 'logo_url'], 'string', 'max' => 240],
			[['return_url_text'], 'string', 'max' => 35],
			[['return_url_target', 'cancel_url_target'], 'in', 'range' => ['_top', '_blank', '_self', '_parent']],
			[['status_url', 'status_url2'], 'string', 'max' => 400],
			[['language'], 'string', 'length' => 2],
			[['language'], 'in', 'range' => explode(',', SkrillQuickCheckout::LANGUAGES)],
			[['prepare_only'], 'in', 'range' => [0, 1]],
			[['dynamic_descriptor'], 'string', 'max' => 50],
			[['merchant_fields'], MerchantFieldsValidator::className()],
			[['amount'], 'number'],
			[['amount'], 'filter', 'filter' => function($value) {
				//Strip trailing zeros (requirement of skrill)
				if (strpos($value, '.') !== false) {
				return preg_replace("/(0+$)|(\.0+$)/", '', $value);
				} else {
					return $value;
				}
			}],
			[['currency'], 'string', 'length' => 3],
			[['currency'], 'in', 'range' => explode(',', SkrillQuickCheckout::CURRENCIES)],
		];
	}


	/**
	 * 
	 * @return SkrillQuickCheckout
	 */
	public function getComponent()
	{
		return $this->_component;
	}


	public function setComponent(SkrillQuickCheckout $cmp)
	{
		$this->_component = $cmp;
	}

	/**
	 * 
	 * @return aarray
	 */
	public function getAttributesWithValues()
	{
		$result = [];
		foreach ($this->attributes as $attribute => $value) {
			if (!empty($value)) {
				$result[$attribute] = $value;
			}
		}
		return $result;
	}
	
	
	public function renderHiddenFields($attributes = null)
	{
		if (null == $attributes) {
			$attributes = $this->getAttributesWithValues();
		}
		
		$result = '';
		
		foreach ($attributes as $k => $v) {
			if ($k != 'merchant_fields') {
				$result .= \yii\helpers\Html::hiddenInput($k, $v);
			} else {
				$result .= \yii\helpers\Html::hiddenInput('merchant_fields', implode(',', array_keys($this->merchant_fields)));
				
				foreach ($this->merchant_fields as $field => $value) {
					$result .= \yii\helpers\Html::hiddenInput($field, $value);
				}
			}
		}
		
		return $result;
	}
	
}
