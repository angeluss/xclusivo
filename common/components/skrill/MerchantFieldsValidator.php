<?php


namespace common\components\skrill;

use Yii;
use yii\validators\Validator;
use yii\validators\StringValidator;


class MerchantFieldsValidator extends Validator
{


	public function validateAttribute($model, $attribute)
	{
		if (!is_array($model->$attribute)) {
			$this->addError($model, $attribute, '{attribute} must be an array.');
			return;
		}

		foreach ($model->$attribute as $key => $value) {
			$validator = new StringValidator();
			$validator->max = SkrillQuickCheckout::MERCHANT_FIELDS_MAX_LENGTH;
			
			if (!$validator->validate($value)) {
				$this->addError($model, $attribute, 'Value "' . $key . '" in {attribute} should contain at most '.SkrillQuickCheckout::MERCHANT_FIELDS_MAX_LENGTH.' characters.');
			}
		}
	}


}
