<?php


namespace common\behaviors;

use yii;
use yii\base\Behavior;
use yii\base\Exception;
use yii\db\ActiveRecord;
use common\models\Score;
use common\models\ScoreQuery;


/**
 * @property \yii\db\ActiveRecord $owner 
 */
class ScoringEntityBehavior extends Behavior
{

	public $separator = '|';


	public function attach($owner)
	{
		if (!($owner instanceof yii\base\Model)) {
			throw new Exception('Behavior owner must be an instance of yii\db\ActiveRecord.');
		}

		parent::attach($owner);
	}


	/**
	 * 
	 * @return string
	 * @throws Exception
	 */
	public function getScoringCustomId()
	{
		if (!$this->owner->primaryKey || $this->owner->isNewRecord) {
			throw new Exception('Record is not saved yet.');
		}

		return $this->owner->className() . $this->separator . $this->owner->primaryKey;
	}


	/**
	 * 
	 * @return \common\models\ScoreQuery
	 */
	public function findScoresByCustomId()
	{
		$query = Score::find()->byCustomId($this->getScoringCustomId());
		return $query;
	}


	/**
	 * 
	 * @return \common\models\ScoreQuery
	 */
	public function findScoresByEntity()
	{
		$query = Score::find()->byEntity($this->owner);
		return $query;
	}


	/**
	 * 
	 * @return \common\models\ScoreQuery
	 */
	public function findScoresByClass($class = null)
	{
		$query = Score::find()->byClass((null === $class) ? $this->owner : $class);
		return $query;
	}


//	public function findScore()

}
