<div class="girls">
	<?php
	echo yii\widgets\ListView::widget([
		'dataProvider' => $provider,
		'layout' => '{items}',
		'itemView' => '_booking_item_for_na',
		'viewParams' => ['isPastBooking' => $isPastBooking]
	]);
	?>
</div>
<?php if ($needShowMore) : ?>

	<a href="#TODO-URL" class="btn"><?php echo Yii::t('app', 'Show More'); ?></a>
<?php endif; ?>
