<?php


namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\Score;
use common\helpers\Scoring;
use yii\helpers\Console;
use frontend\models\Advertiser;
use common\models\User;
use frontend\models\Reviews;
use common\models\Booking;
use common\helpers\Payment;

class BookingController extends Controller
{

	public function actionApproved()
	{
		self::out('Processing unpaid approived booking, timeout: '.Payment::unpaidBookingTimeout().'h');
		$count = 0;
		$bookings = Booking::find()
			->joinWith(['advertiser', 'user'])
			->pastInHours(Payment::unpaidBookingTimeout())
			->approved()
			->notPaid();
		
		foreach ($bookings->each() as $model) {
			if ($model->no_show_guarantee) {
				$model->guarantee();
			} else {
				$model->cancel(true);
			}
			$count++;
		}
		
		self::out($count.' records handled.');
	}


	public function actionPending()
	{
		self::out('Processing unapproved pending booking, expiration timeout: '.\common\helpers\Calendar::eventExpirationLimit().'h');
		
		$count = 0;
		$bookings = Booking::find()
			->joinWith(['advertiser', 'user'])
			->notAnswered();
		
		foreach ($bookings->each() as $model) {
			$model->cancel(true);
			$count++;
		}
		self::out($count.' records handled.');
	}
	
	
	/**
	 * 
	 * @param string $string
	 * @param int $level
	 * @return void
	 */
	protected static function out($string, $level = 0)
	{
		$prefix = str_pad('', $level, "\t");
		echo $prefix . $string . "\r\n";
	}
}
