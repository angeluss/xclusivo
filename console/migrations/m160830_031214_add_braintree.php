<?php

use yii\db\Migration;


class m160830_031214_add_braintree extends Migration
{


    public function up()
    {
        $this->alterColumn('{{%transactions}}', 'gateway', 'enum("ManualPayout","Braintree") NULL DEFAULT NULL');
        $this->alterColumn('{{%transactions}}', 'sender_email', $this->string()->null()->defaultValue(null));
        $this->alterColumn('{{%transactions}}', 'receiver_email', $this->string()->null()->defaultValue(null));
    }


    public function down()
    {
        echo "\r\nNo actions needed to step down.\r\n";
    }


    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */

}
