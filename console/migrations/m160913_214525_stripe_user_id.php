<?php

use yii\db\Migration;


class m160913_214525_stripe_user_id extends Migration
{


    public function up()
    {
        $this->alterColumn('{{%transactions}}', 'gateway', 'enum("ManualPayout","Braintree", "Stripe") NULL DEFAULT NULL');
        $this->addColumn('{{%user}}', 'stripe_id', $this->string(20)->null()->defaultValue(null));
    }


    public function down()
    {
        $this->dropColumn('{{%user}}', 'stripe_id');
        $this->alterColumn('{{%transactions}}', 'gateway', 'enum("ManualPayout","Braintree") NULL DEFAULT NULL');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */

}


