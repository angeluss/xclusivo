<?php

use yii\db\Migration;
use yii\db\Schema;


class m160826_022135_like_correct extends Migration
{


	public function up()
	{
		$this->dropForeignKey('fk-likephoto-user_id', '{{%like_photo}}');
		$this->dropForeignKey('fk-likephoto-media_id', '{{%like_photo}}');
		$this->dropTable('{{%like_photo}}');

		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%like_photo}}', [
			'id' => Schema::TYPE_PK,
			'media_id' => 'INT(11) NOT NULL',
			'user_id' => 'INT(11) NOT NULL',
		]);

		$this->createIndex('uniq_media_user', '{{%like_photo}}', ['media_id', 'user_id'], true);
		$this->addForeignKey('fk-likephoto-user_id', '{{%like_photo}}', 'user_id', '{{%user}}', 'id', 'CASCADE');
		$this->addForeignKey('fk-likephoto-media_id', '{{%like_photo}}', 'media_id', '{{%media}}', 'id', 'CASCADE');
	}


	public function down()
	{
		echo "\r\nNo action needed to roll back.\r\n";
	}


	/*
	  // Use safeUp/safeDown to run migration code within a transaction
	  public function safeUp()
	  {
	  }

	  public function safeDown()
	  {
	  }
	 */

}
