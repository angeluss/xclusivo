<?php

use yii\db\Migration;
use yii\db\Schema;

class m160903_043612_refund extends Migration
{
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $tableName = '{{%refunds}}';

        $this->createTable($tableName, [
            'id' => Schema::TYPE_PK,
            'user_id' => $this->integer()->null()->defaultValue(null),
            'transaction_id' => $this->integer()->notNull(),
            'user_email' => $this->string()->notNull(),
            'amount' => $this->decimal(10, 2)->notNull(),
            'description' => $this->text()->notNull(),
            'admin_comment' => $this->text()->null(),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
            'status' => 'enum("Pending", "Completed", "Cancelled") NOT NULL DEFAULT "Pending"',
            ], $tableOptions);

        $this->createIndex('idx_user_email', $tableName, 'user_email');
        $this->addForeignKey('fk_refund_user', $tableName, 'user_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_refund_transaction', $tableName, 'transaction_id', '{{%transactions}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $tableName = '{{%refunds}}';
        $this->dropForeignKey('fk_refund_user', $tableName);
        $this->dropForeignKey('fk_refund_transaction', $tableName);
        $this->dropTable($tableName);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
