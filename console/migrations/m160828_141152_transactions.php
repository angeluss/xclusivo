<?php

use yii\db\Migration;
use yii\db\Schema;


class m160828_141152_transactions extends Migration
{


    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $tableName = '{{%transactions}}';

        $this->createTable($tableName, [
            'id' => Schema::TYPE_PK,
            'external_id' => $this->string(30)->null()->defaultValue(null),
            'sender_id' => $this->integer()->null()->defaultValue(null),
            'receiver_id' => $this->integer()->null()->defaultValue(null),
            'sender_email' => $this->string()->notNull(),
            'receiver_email' => $this->string()->notNull(),
            'amount' => $this->decimal(10, 2)->notNull(),
            'gateway' => 'enum("ManualPayout","Skrill") NULL DEFAULT NULL',
            'status' => 'enum("Pending", "Completed", "Cancelled", "Reverted") NOT NULL DEFAULT "Pending"',
            'reason' => $this->string()->notNull(),
            'comment' => $this->text(),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
            'is_test' => $this->boolean(),
            ], $tableOptions);

        $this->createIndex('idx_gateway', $tableName, 'gateway');
        $this->createIndex('idx_externai_id', $tableName, 'external_id');
        $this->createIndex('idx_sender_email', $tableName, 'sender_email');
        $this->createIndex('idx_receiver_email', $tableName, 'receiver_email');

        $this->addForeignKey('fk_sender', $tableName, 'sender_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_receiver', $tableName, 'receiver_id', '{{%user}}', 'id', 'SET NULL', 'CASCADE');
    }


    public function down()
    {
        $tableName = '{{%transactions}}';
        $this->dropForeignKey('fk_sender', $tableName);
        $this->dropForeignKey('fk_receiver', $tableName);
        $this->dropTable($tableName);
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */

}


