<?php

use yii\db\Migration;

class m160828_131556_no_show extends Migration
{
    public function up()
    {
		$this->addColumn('{{%bookings}}', 'no_show_guarantee', $this->boolean());
    }

    public function down()
    {
		$this->dropColumn('{{%bookings}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
