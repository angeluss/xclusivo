<?php

use yii\db\Migration;

class m160828_010426_booking_cost extends Migration
{
    public function up()
    {
		$this->delete('{{%bookings}}');
		$this->addColumn('{{%bookings}}', 'total_cost', $this->decimal(10,2)->notNull());
    }

    public function down()
    {
		$this->dropColumn('{{%bookings}}', 'total_cost');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
