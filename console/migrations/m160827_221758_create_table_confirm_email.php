<?php

use yii\db\Migration;

class m160827_221758_create_table_confirm_email extends Migration
{
    public function up()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%confirm_email}}', [
          'user_id' => 'INT(11) NOT NULL',
          'hash' => 'VARCHAR(32) NOT NULL',
          'PRIMARY KEY (`user_id`)'
      ]);

      $this->addForeignKey('fk-confirmemail-user_id', '{{%confirm_email}}', 'user_id', '{{%user}}', 'id', 'CASCADE');
    }

    public function down()
    {
      $this->dropForeignKey('fk-confirmemail-user_id', '{{%confirm_email}}');
      $this->dropTable('{{%confirm_email}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
