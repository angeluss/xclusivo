<?php

use yii\db\Migration;

class m160823_121907_change_eye_colors extends Migration
{
    public function up()
    {
        $this->delete('{{%colors}}', 'type = 2 AND title = "Honey"');
        $this->delete('{{%colors}}', 'type = 2 AND title = "Amethyst"');
        $this->insert('{{%colors}}', ['type' => 2, 'title' => 'Blue', 'class' => 'color4']);
    }

    public function down()
    {
      $this->insert('{{%colors}}', ['type' => 2, 'title' => 'Honey', 'class' => 'color12']);
      $this->insert('{{%colors}}', ['type' => 2, 'title' => 'Amethyst', 'class' => 'color12']);
      $this->delete('{{%colors}}', 'type = 2 AND title = "Blue"');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
