<?php

use yii\db\Migration;
use frontend\models\Nationality;

class m160828_184649_nationality_correction_ASC extends Migration
{
    public function up()
    {
        $nationality_data = Nationality::loadModel();
        $this->dropForeignKey('advertiser_ibfk_5','{{%advertiser}}');
        
        $this->delete('{{%nationality}}');
        $this->dropTable('{{%nationality}}');
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%nationality}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string('255')->notNull()->defaultValue(''),

        ], $tableOptions);
        
        foreach($nationality_data as $value)
        {
            
            $this->insert('{{%nationality}}',array('title'=>$value));
            
        }
        
        $this->addForeignKey('advertiser_ibfk_5','{{%advertiser}}','nationality_id','{{%nationality}}','id','set null','cascade');
    }

    public function down()
    {
        $this->dropForeignKey('advertiser_ibfk_5','{{%advertiser}}');
        
        $this->delete('{{%nationality}}');
        
        $this->execute("INSERT INTO `nationality` (`id`, `title`) VALUES (1, 'Danish'),
            (2, 'British/English'),
            (3, 'Estonian'),
            (4, 'Finnish'),
            (5, 'Icelandic'),
            (6, 'Irish'),
            (7, 'Latvian'),
            (8, 'Lithuanian'),
            (9, 'British / Northern Irish'),
            (10, 'Norwegian'),(11, 'British / Scottish'),(12, 'Swedish'),(13, 'British'),(14, 'British / Welsh'),(16, 'Austrian'),
            (17, 'Belgian'),(18, 'French'),(19, 'German'),(20, 'Dutch'),(21, 'Swiss'),(23, 'Albanian'),(24, 'Croatian'),(25, 'Cypriot'),
            (26, 'Greek'),(27, 'Italian'),(28, 'Portuguese'),(29, 'Serbian'),(30, 'Slovenian / Slovene'),(31, 'Spanish'),(33, 'Belarusian'),
            (34, 'Bulgarian'),(35, 'Czech'),(36, 'Hungarian'),(37, 'Polish'),(38, 'Romanian'),(39, 'Russian'),(40, 'Slovak / Slovakian'),
            (41, 'Ukrainian'),(43, 'Canadian'),(44, 'Mexican'),(45, 'American'),(47, 'Cuban'),(48, 'Guatemalan'),(49, 'Jamaican'),
            (51, 'Argentine / Argentinian'),(52, 'Bolivian'),(53, 'Brazilian'),(54, 'Chilean'),(55, 'Colombian'),(56, 'Ecuadorian'),
            (57, 'Paraguayan'),(58, 'Peruvian'),(59, 'Uruguayan'),(60, 'Venezuelan'),(62, 'Georgian'),(63, 'Iranian'),(64, 'Iraqi'),
            (65, 'Israeli'),(66, 'Jordanian'),(67, 'Kuwaiti'),(68, 'Lebanese'),(69, 'Palestinian'),(70, 'Saudi Arabian'),(71, 'Syrian'),
            (72, 'Turkish'),(73, 'Yemeni / Yemenite'),(75, 'Afghan / Afghani'),(76, 'Bangladeshi'),(77, 'Indian'),
            (78, 'Kazakh / Kazakhstani'),(79, 'Nepalese / Nepali'),(80, 'Pakistani'),(81, 'Sri Lankan'),(83, 'Chinese'),
            (84, 'Japanese'),(85, 'Mongolian'),(86, 'North Korean'),(87, 'South Korean'),(88, 'Taiwanese'),(90, 'Cambodian'),
            (91, 'Indonesian'),(92, 'Laotian / Lao'),(93, 'Malaysian'),(94, 'Burmese'),(95, 'Filipino'),(96, 'Singaporean'),
            (97, 'Thai'),(98, 'Vietnamese'),(100, 'Australian'),(101, 'Fijian'),(102, 'New Zealand'),(104, 'Algerian'),
            (105, 'Egyptian'),(106, 'Ghanaian'),(107, 'Ivorian'),(108, 'Libyan'),(109, 'Moroccan'),(110, 'Nigerian'),
            (111, 'Tunisian'),(113, 'Ethiopian'),(114, 'Kenyan'),(115, 'Somali / Somalian'),(116, 'Sudanese'),
            (117, 'Tanzanian'),(118, 'Ugandan'),(120, 'Angolan'),(121, 'Botswanan'),(122, 'Congolese'),(123, 'Malagasy'),
            (124, 'Mozambican'),(125, 'Namibian'),(126, 'South African'),(127, 'Zambian'),(128, 'Zimbabwean')");
        
        $this->addForeignKey('advertiser_ibfk_5','{{%advertiser}}','nationality_id','{{%nationality}}','id','set null','cascade');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
