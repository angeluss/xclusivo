<?php

use yii\db\Migration;

class m160904_155215_add_column_sort_to_refunds extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE {{%refunds}} ADD `sort_order` TINYINT(1) DEFAULT 0");
    }

    public function down()
    {
        $this->dropColumn('{{%refunds}}', 'sort_order');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
