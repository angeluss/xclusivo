<?php

use yii\db\Migration;


class m160828_003138_balance extends Migration
{


	public function up()
	{
		$this->addColumn('{{%user}}', 'balance', $this->decimal(10, 2)->notNull()->defaultValue(0));
	}


	public function down()
	{
		$this->dropColumn('{{%user}}', 'balance');
	}


	/*
	  // Use safeUp/safeDown to run migration code within a transaction
	  public function safeUp()
	  {
	  }

	  public function safeDown()
	  {
	  }
	 */

}
