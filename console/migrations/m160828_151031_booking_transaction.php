<?php

use yii\db\Migration;


class m160828_151031_booking_transaction extends Migration
{


	public function up()
	{
		$this->addColumn('{{%bookings}}', 'transaction_id', $this->integer()->null()->defaultValue(null));
		$this->addForeignKey('fk_booking_transaction', '{{%bookings}}', 'transaction_id', '{{%transactions}}', 'id', 'SET NULL', 'CASCADE');
	}


	public function down()
	{
		$this->dropForeignKey('fk_booking_transaction', '{{%bookings}}');
		$this->dropColumn('{{%bookings}}', 'transaction_id', $this->integer()->null()->defaultValue(null));
	}


	/*
	  // Use safeUp/safeDown to run migration code within a transaction
	  public function safeUp()
	  {
	  }

	  public function safeDown()
	  {
	  }
	 */

}
