<?php

use yii\db\Migration;

class m160823_214316_create_table_like_photo extends Migration
{
    public function up()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%like_photo}}', [
          'media_id' => 'INT(11) NOT NULL',
          'user_id' => 'INT(11) NOT NULL',
          'PRIMARY KEY (`user_id`, `media_id`)'
      ]);

      $this->addForeignKey('fk-likephoto-user_id', '{{%like_photo}}', 'user_id', '{{%user}}', 'id', 'CASCADE');
      $this->addForeignKey('fk-likephoto-media_id', '{{%like_photo}}', 'media_id', '{{%media}}', 'id', 'CASCADE');
    }

    public function down()
    {
      $this->dropForeignKey('fk-likephoto-user_id', '{{%like_photo}}');
      $this->dropForeignKey('fk-likephoto-media_id', '{{%like_photo}}');
      $this->dropTable('{{%like_photo}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
