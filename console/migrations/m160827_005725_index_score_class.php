<?php

use yii\db\Migration;

class m160827_005725_index_score_class extends Migration
{
    public function up()
    {
		$this->createIndex('idx_entity_class', '{{%scores}}', 'entity_class');
    }

    public function down()
    {
		$this->dropIndex('idx_entity_class', '{{%scores}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
