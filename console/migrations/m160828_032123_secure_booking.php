<?php

use yii\db\Migration;

class m160828_032123_secure_booking extends Migration
{
    public function up()
    {
		$this->addColumn('{{%bookings}}', 'is_secure', $this->boolean());
		$this->addColumn('{{%bookings}}', 'secure_code', $this->string(10)->null()->defaultValue(null));
    }

    public function down()
    {
		$this->dropColumn('{{%bookings}}', 'is_secure', $this->boolean());
		$this->dropColumn('{{%bookings}}', 'secure_code', $this->string(10)->null()->defaultValue(null));
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
